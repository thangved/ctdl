#include "treequeue.h"

void levelOrder(AVLTree root)
{
    if (root == NULL)
        return;
    treequeue queue = makenullqueue();
    enqueue(root, &queue);

    while (!emptyqueue(queue))
    {
        AVLNode node = front(queue);
        if (node != NULL)
        {
            printf("(%d, %d, %d) ", node->key, node->bal, node->height);
            enqueue(node->left, &queue);
            enqueue(node->right, &queue);
        }
        dequque(&queue);
    }
}