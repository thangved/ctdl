#include "avltree.h"

int main()
{
    AVLNode root = createAVLNode(1);
    insertNode(4, &root);
    insertNode(5, &root);
    insertNode(1, &root);
    insertNode(3, &root);
    insertNode(32, &root);
    insertNode(31, &root);
    insertNode(30, &root);
    insertNode(302, &root);

    levelOrder(root);
    return 0;
}