#include <stdlib.h>
#include <stdio.h>

typedef int ElementType;

const int BALANCE = 0;
const int LEFT = 1;
const int RIGHT = 2;

typedef struct Node
{
    ElementType key;
    int height;
    int bal;
    struct Node *left, *right;
} * AVLTree;

typedef AVLTree AVLNode;

AVLNode createAVLNode(ElementType x)
{
    AVLNode node = (AVLNode)malloc(sizeof(struct Node));
    node->key = x;
    node->bal = BALANCE;
    node->height = 0;
    node->left = NULL;
    node->right = NULL;
    return node;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

int height(AVLNode node)
{
    return node == NULL ? -1 : 1 + max(height(node->left), height(node->right));
}

void updateBalance(AVLNode *pnode)
{
    if (*pnode == NULL)
        return;
    updateBalance(&(*pnode)->left);
    updateBalance(&(*pnode)->right);
    int bal = height((*pnode)->left) - height((*pnode)->right);

    if (bal <= 1 && bal >= -1)
        (*pnode)->bal = BALANCE;
    if (bal > 1)
        (*pnode)->bal = LEFT;
    if (bal < -1)
        (*pnode)->bal = RIGHT;
}

void updateHeight(AVLNode *pnode)
{
    if (*pnode == NULL)
        return;
    updateHeight(&((*pnode)->left));
    updateHeight(&((*pnode)->right));
    (*pnode)->height = height(*pnode);
}

void rotateLeft(AVLNode *pnode)
{
    AVLNode left = (*pnode)->left;
    AVLNode rightleft = left->right;
    left->right = (*pnode);
    (*pnode)->left = rightleft;
    (*pnode) = left;
    updateHeight(pnode);
    updateBalance(pnode);
}

void rotateRight(AVLNode *pnode)
{
    AVLNode right = (*pnode)->right;
    AVLNode leftright = right->left;
    right->left = (*pnode);
    (*pnode)->right = leftright;
    (*pnode) = right;
    updateHeight(pnode);
    updateBalance(pnode);
}

void rotateLeftRight(AVLNode *pnode)
{
    rotateLeft(&((*pnode)->left));
    rotateRight(pnode);
}

void rotateRightLeft(AVLNode *pnode)
{
    rotateRight(&((*pnode)->right));
    rotateLeft(pnode);
}

int getBalance(AVLNode node)
{
    return node == NULL ? 0 : height(node->left) - height(node->right);
}

void insertNode(ElementType x, AVLTree *proot)
{
    if (*proot == NULL)
    {
        (*proot) = createAVLNode(x);
        return;
    }
    if ((*proot)->key == x)
        return;
    if (x < (*proot)->key)
        insertNode(x, &((*proot)->left));
    if (x > (*proot)->key)
        insertNode(x, &((*proot)->right));

    updateHeight(proot);
    updateHeight(proot);
    if ((*proot)->bal == BALANCE)
        return;
    if ((*proot)->bal == LEFT)
    {
        if (getBalance((*proot)->left) > 0)
            rotateRight(proot);
        else
            rotateLeftRight(proot);
    }
    if ((*proot)->bal == RIGHT)
    {
        if (getBalance((*proot)->left) < 0)
            rotateLeft(proot);
        else
            rotateRightLeft(proot);
    }
    updateHeight(proot);
    updateBalance(proot);
}