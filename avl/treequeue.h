#include "define.h"

#define true 1;
#define false 0;
typedef int boolean;

typedef struct node
{
    AVLNode element;
    struct node *next;
} * position;

typedef struct
{
    position front, rear;
} treequeue;

treequeue makenullqueue()
{
    treequeue q;
    q.front = (position)malloc(sizeof(struct node));
    q.front->next = NULL;
    q.rear = q.front;
    return q;
}

boolean emptyqueue(treequeue q)
{
    return q.front == q.rear;
}

AVLNode front(treequeue q)
{
    return q.front->next->element;
}

void dequque(treequeue *q)
{
    if (emptyqueue(*q))
        return;
    position temp = q->front;
    q->front = q->front->next;
    free(temp);
}

void enqueue(AVLNode node, treequeue *q)
{
    position p = (position)malloc(sizeof(struct node));
    p->element = node;
    p->next = NULL;
    (*q).rear->next = p;
    (*q).rear = p;
}
