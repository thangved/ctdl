#include "Stack.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

char *strrev(char *s)
{
    int len = strlen(s);
    char *rs;
    rs = (char *)malloc(len * sizeof(char));
    strcpy(rs, s);
    for (int i = 0; i < len / 2; i++)
    {
        rs[i] = s[len - i - 1];
        rs[len - i - 1] = s[i];
    }

    return rs;
}

int isnum(char c)
{
    return c >= '0' && c <= '9';
}
int isop(char c)
{
    return (c == '+' || c == '-' || c == '*' || c == '/');
}

float calc(float a, float b, char op)
{
    switch (op)
    {
    case '+':
        return a + b;
    case '-':
        return b - a;
    case '*':
        return a * b;

    default:
        return b - a;
    }
}

// Tinh gia tri cua chuoi hau to
float poststringtovalue(char *pos)
{
    Stack S;
    makenullStack(&S);
    for (int i = 0; i < strlen(pos); i++)
    {
        if (isop(pos[i]))
        {
            float el = top(S);
            pop(&S);
            float er = top(S);
            pop(&S);

            push(calc(el, er, pos[i]), &S);
        }
        if (isnum(pos[i]))
            push((float)(pos[i] - 48), &S);
    }
    return top(S);
}

// Tinh gia tri cua chuoi tien to
float prestringtovalue(char *pre)
{
    return poststringtovalue(strrev(pre));
}

int main()
{
    // Test
    char pre[] = "* + 6 4 5";
    char pos[] = "6 4 5 + *";

    printf("pre: \"%s\", value: %.2f\n", pre, prestringtovalue(pre));
    printf("pos: \"%s\", value: %.2f\n", pos, poststringtovalue(pos));
    return 0;
}